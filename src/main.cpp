#include "Application.h"

int main(int argc, char *argv[])
{
    Application app;
    app.buildAndRun(argc, argv);
    return app.exitCode();
}
