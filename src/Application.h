#ifndef APPLICATION_H
#define APPLICATION_H

#include "ServiceManager.h"
#include "Menu.h"

class Application
{
public:
    Application();

    // run application
    void buildAndRun(int argc, char *argv[]);

    // return a result of program
    int exitCode() const;
private:
    void addNcursesView(spm::ServiceManager& manager) const;
    void addSdl2View(spm::ServiceManager& manager) const;
    // result of executable program
    int m_exitCode;
};

#endif // APPLICATION_H
