#include "Application.h"
#include "TestLogic.h"
#ifdef SPM_NCURSES
#include "NcursesView.h"
#endif
#ifdef SPM_SDL2
#include "Sdl2View.h"
#endif
#include "ServiceManager.h"
#include "Tuning.h"
#include "Logger.h"
#include <iostream>
#include <locale>

Application::Application():
    m_exitCode(0)
{
}

void Application::buildAndRun(int argc, char *argv[])
{
    std::setlocale(LC_ALL, "");
    spm::ServiceManager manager;
    spm::Tuning tunning;
    // EAFP principle
    // settings
    try
    {
        spm::Logger::redirectToFile("log");
        spm::Logger::setLowType(spm::Logger::Type::TRACE);
        spm::Logger::addFlag( spm::LoggerFlag::MILLISECONS );
        tunning.parse(argc, const_cast<const char**>(argv));
    }
    catch(std::logic_error e)
    {
        m_exitCode = 4;
        std::cerr << tunning.getProgramName() << ": " << e.what() << std::endl;
        tunning.printHelp("", std::cerr);
        return;
    }
    // logic
    try
    {
        // add services
        TestLogic* testLogic = new TestLogic(manager);
        manager.addService(testLogic);
        // add view
        addNcursesView(manager);
        addSdl2View(manager);

        //NcursesView* ncursesView = new NcursesView(manager);
        //ncursesView->setMenu(makeMainMenu(), NcursesView::MenuType::MAIN);
        //ncursesView->setMenu(makePauseMenu(), NcursesView::MenuType::PAUSE);
        //manager.addService(ncursesView);

        //Sdl2View* sdl2View = new Sdl2View(manager);
        //manager.addService(sdl2View);

        // run services
        m_exitCode = manager.run() ? 0 : 1;
    }
    catch(std::logic_error e)
    {
        m_exitCode = 2;
        std::cerr << tunning.getProgramName() << ": " << e.what() << std::endl;
    }
    catch(...)
    {
        m_exitCode = 3;
        std::cerr << tunning.getProgramName() << ": unknown error" << std::endl;
    }
}

int Application::exitCode() const
{
    return m_exitCode;
}

void Application::addNcursesView(spm::ServiceManager& manager) const
{
#ifdef SPM_NCURSES
    NcursesView* ncursesView = new NcursesView(manager);
    ncursesView->setMenu( spm::Menu("Main", {
                                        spm::Menu("Старт").addAction(spm::MenuManager::Action::CLOSE_MENU),
                                        spm::Menu("Exit").addAction(NcursesView::Action::EXIT).addAction(spm::MenuManager::Action::CLOSE_MENU),
                                        spm::Menu("Settings", {
                                            spm::Menu("Fullscreen", {
                                                spm::Menu("Enable").addAction(spm::MenuManager::CLOSE_MENU),
                                                spm::Menu("Disable").addAction(spm::MenuManager::CLOSE_MENU)
                                            }).setVariable(),
                                            spm::Menu("Sound", {
                                                spm::Menu("Enable").addAction(spm::MenuManager::CLOSE_MENU),
                                                spm::Menu("Disable").addAction(spm::MenuManager::CLOSE_MENU)
                                            }).setVariable(),
                                            spm::Menu("Name").setVariable(),
                                            spm::Menu("Back").addAction(spm::MenuManager::CLOSE_MENU)
                                        })
                                    }), NcursesView::MenuType::MAIN );
    ncursesView->setMenu( spm::Menu("Pause", {
                                        spm::Menu("Resume").addAction(spm::MenuManager::Action::CLOSE_MENU),
                                        spm::Menu("Main menu").addAction(spm::MenuManager::Action::CLOSE_MENU),
                                        spm::Menu("Exit").addAction(NcursesView::Action::EXIT).addAction(spm::MenuManager::Action::CLOSE_MENU)
                                    }), NcursesView::MenuType::MAIN );
    manager.addService(ncursesView);
#endif
}

void Application::addSdl2View(spm::ServiceManager& manager) const
{
#ifdef SPM_SDL2
    Sdl2View* sdl2View = new Sdl2View(manager);
    manager.addService(sdl2View);
#endif
}
