#include "TestLogic.h"
#include <string.h>

TestLogic::TestLogic(spm::ServiceManager &manager):
    BaseService("logic"),
    m_manager(&manager),
    m_running(false)
{
    addSupportedType("view");
}

void TestLogic::initialize()
{
    m_running = true;

    // create area
    m_area = spm::Variant::Dict{
        {"event", "area"},
        {"width", 5},
        {"height", 5},
        {"place", spm::Variant::Data(25*4, 0)}
    };
    m_place = reinterpret_cast<int32_t*>(m_area.getDict()["place"].getData().data());

    m_place[2] = 1;

    m_manager->performAction("logic", m_area);
}

bool TestLogic::tick()
{
    return m_running;
}

void TestLogic::action(const std::string &type, const spm::Variant &data)
{
    if(type == "view")
    {
        const spm::Variant::Dict& args = data.toDict();
        if(args.at("event") == "exit")
        {
            m_running = false;
        }
    }
}
