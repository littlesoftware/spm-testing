#ifndef TESTLOGIC_H
#define TESTLOGIC_H

#include "BaseService.h"
#include "ServiceManager.h"

class TestLogic: public spm::BaseService
{
public:
    explicit TestLogic(spm::ServiceManager& manager);

    virtual void initialize() override;
    virtual bool tick() override;
    virtual void action(const std::string& type, const spm::Variant& data) override;

private:
    // -------------------
    // MEMORY DATA
    // -------------------
    spm::ServiceManager* m_manager;
    bool                 m_running;
    spm::Variant         m_area;
    int32_t*             m_place;
};

#endif // TESTLOGIC_H
